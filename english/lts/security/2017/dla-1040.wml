<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-11521">CVE-2017-11521</a>

    <p>The SdpContents::Session::Medium::parse function in
    resip/stack/SdpContents.cxx in reSIProcate 1.10.2 allows remote
    attackers to cause a denial of service (memory consumption) by
    triggering many media connections.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.8.5-4+deb7u1.</p>

<p>We recommend that you upgrade your resiprocate packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1040.data"
# $Id: $
