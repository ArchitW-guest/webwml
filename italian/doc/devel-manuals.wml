#use wml::debian::ddp title="Manuali Debian per gli sviluppatori"
#include "$(ENGLISHDIR)/doc/manuals.defs"
#include "$(ENGLISHDIR)/doc/devel-manuals.defs"
#use wml::debian::translation-check translation="89c34d78bbbe7a447c9578651b0f0c0e3ba86140" maintainer="Francesca Ciceri"
#first translator and maintainer="Johan Haggi"

<document "Debian Policy Manual" "policy">

<div class="centerblock">
<p>
  Questo manuale descrive le linee guida (policy) per la distribuzione
  Debian GNU/Linux. Esse descrivono la struttura ed i contenuti di un
  archivio Debian, alcune problematiche dipendenti dal sistema operativo
  ed i requisiti tecnici che ogni pacchetto deve soddisfare per essere
  incluso nella distribuzione.

<doctable>
  <authors "Ian Jackson, Christian Schwarz, David A. Morris">
  <maintainer "The Debian Policy group">
  <status>
  pronto
  </status>
  <availability>
  <inpackage "debian-policy">
  <inddpsvn-debian-policy>
  <p>
    <a href="https://bugs.debian.org/debian-policy">Correzioni proposte</a> alle linee guida
  </p>


  <p>Documentazione supplementare sulle linee guida:</p>
  <ul>
    <li><a href="packaging-manuals/fhs/fhs-3.0.html">Gerarchia standard del filesystem (Filesystem Hierarchy Standard)</a>
    [<a href="packaging-manuals/fhs/fhs-3.0.pdf">PDF</a>]
    [<a href="packaging-manuals/fhs/fhs-3.0.txt">testo puro</a>]</li>
    <li><a href="debian-policy/#document-upgrading-checklist">Lista di controllo degli aggiornamenti</a></li>
    <li><a href="packaging-manuals/virtual-package-names-list.txt">Elenco dei nomi dei pacchetti virtuali</a></li>
    <li><a href="packaging-manuals/menu-policy/">Linee guida per i menù</a>
    [<a href="packaging-manuals/menu-policy/menu-policy.txt.gz">testo puro</a>]</li>
    <li><a href="packaging-manuals/perl-policy/">Linee guida per Perl</a>
    [<a href="packaging-manuals/perl-policy/perl-policy.txt.gz">testo puro</a>]</li>
    <li><a href="packaging-manuals/debconf_specification.html">Specifiche per debconf</a></li>
    <li><a href="packaging-manuals/debian-emacs-policy">Linee guida per Emacs</a></li>
    <li><a href="packaging-manuals/java-policy/">Linee guida per Java</a></li>
    <li><a href="packaging-manuals/python-policy/">Linee guida per Python</a></li>
    <li><a href="packaging-manuals/copyright-format/1.0/">specifiche per il formato del file copyright</a></li>
  </ul>
  </availability>
</doctable>
</div>

<hr>

<document "Debian Developer's Reference" "devref">

<div class="centerblock">
<p>
  Questo manuale descrive le procedure e le risorse per i manutentori Debian.
  Spiega come diventare un nuovo sviluppatore, le procedure di invio, come
  utilizzare il nostro bug tracking system, le mailing list, i server
  Internet, ecc.

  <p>Questo manuale è pensato come una <em>guida di riferimento</em> per tutti gli
  sviluppatori Debian (sia nuovi che vecchi).

<doctable>
  <authors "Ian Jackson, Christian Schwarz, Lucas Nussbaum, Rapha&euml;l Hertzog, Adam Di Carlo, Andreas Barth">
  <maintainer "Lucas Nussbaum, Hideki Yamane, Holger Levsen">
  <status>
  pronto
  </status>
  <availability>
  <inpackage "developers-reference">
  <inddpsvn-developers-reference>
  </availability>
</doctable>
</div>

<hr>

<document "Guide for Debian Maintainers" "debmake-doc">

<div class="centerblock">
<p>
Questo documento descirve la compilazione di un pacchetto Debian agli utenti
e futuri sviluppatori usando il comando <code>debmake</code>.
</p>
<p>
È focalizzato sul moderno stile di pacchettizzazione e contiene molti semplici
esempi.
</p>
<ul>
<li>Pacchettizzazione di script POSIX shell</li>
<li>Pacchettizzazione di script Python3</li>
<li>C con Makefile/Autotools/CMake</li>
<li>Pacchetti binari multipli con librerie condivise ecc.</li>
</ul>
<p>
Questa <q>Guide for Debian Maintainers</q> può essere considerata come il
successore di <q>Debian New Maintainers’ Guide</q>.
</p>

<doctable>
  <authors "Osamu Aoki">
  <maintainer "Osamu Aoki">
  <status>
  pronto
  </status>
  <availability>
  <inpackage "debmake-doc">
  <inddpsvn-debmake-doc>
  </availability>
</doctable>
</div>

<hr>

<document "Debian New Maintainers' Guide" "maint-guide">

<div class="centerblock">
<p>
  Questo documento prova a descrivere la costruzione di un pacchetto Debian GNU/Linux
  al comune utente Debian (ed agli aspiranti sviluppatori) con un linguaggio semplice e
  fornendo degli esempi pratici.
</p>
  <p>Diversamente dai tentativi precedenti, questo manuale è basato su <code>debhelper</code>
  e sui nuovi strumenti disponibili per i manutentori.  L'autore sta facendo il possibile
  per incorporare ed unificare le opere precedenti.
</p>
<doctable>
  <authors "Josip Rodin, Osamu Aoki">
  <maintainer "Osamu Aoki">
  <status>
  pronto
  </status>
  <availability>
  <inpackage "maint-guide">
  <inddpsvn-maint-guide>
  </availability>
</doctable>
</div>

<hr>

<document "Introduction to Debian packaging" "packaging-tutorial">

<div class="centerblock">

<p>
Un tutorial introduttivo alla pacchettizzazione in Debian, volto a
insegnare ai futuri sviluppatori come modificare i pacchetti esistenti,
creare i propri pacchetti a come interagire correttamente con la comunità
Debian.
Oltre alla guida vera e propria comprende tre sessioni pratiche su come
modificare il pacchetto <code>grep</code>, pacchettizzare il gioco
<code>gnujump</code> e una libreria Java.
</p>

<doctable>
  <authors "Lucas Nussbaum">
  <maintainer "Lucas Nussbaum">
  <status>
  pronto
  </status>
  <availability>
  <inpackage "packaging-tutorial">
  <inddpsvn-packaging-tutorial>
   </availability>
</doctable>
</div>

<hr>

<document "Debian Menu System" "menu">

<div class="centerblock">
<p>
  Questo manuale descrive il sistema Debian per i menù (Debian Menu System) ed il
  pacchetto <strong>menu</strong>.
</p>
  <p>Il pacchetto menu è stato ispirato dal programma install-fvwm2-menu
  del vecchio pacchetto fvwm2. menu cerca di fornire un'interfaccia
  comune per la costruzione dei menù. Con il comando update-menus di questo
  pacchetto, nessun pacchetto deve più essere modificato per ogni window manager di X,
  inoltre fornisce un'interfaccia unificata per i programmi, siano essi testuali o per
  X.
</p>
<doctable>
  <authors "Joost Witteveen, Joey Hess, Christian Schwarz">
  <maintainer "Joost Witteveen">
  <status>
  pronto
  </status>
  <availability>
  <inpackage "menu">
  <a href="packaging-manuals/menu.html/">HTML online</a>
  </availability>
</doctable>
</div>

<hr>

<document "Introduction to i18n" "i18n">

<div class="centerblock">
<p>
  Questo documento descrive le idee basilari ed il howto di l10n
  (localizzazione), di i18n (internazionalizzazione) e di m17n
  (multilinguaggio) per i programmatori ed i manutentori dei pacchetti.
</p>
  <p>Lo scopo di questo documento è di far sì che sempre più pacchetti
  supportino i18n e che Debian sia sempre più una distribuzione internazionale.
  Contributi da tutto il mondo saranno benvenuti, poiché
  l'autore originale parla giapponese e questo documento potrebbe essere
  solo sulla giapponesizzazione se non ci saranno contributi.
</p>
<doctable>
  <authors "Tomohiro KUBOTA (&#20037;&#20445;&#30000;&#26234;&#24195;)">
  <maintainer "Tomohiro KUBOTA (&#20037;&#20445;&#30000;&#26234;&#24195;)">
  <status>
  in sviluppo
  </status>
  <availability>
  non ancora completato
  <inddpsvn-intro-i18n>
  </availability>
</doctable>
</div>

<hr>

<document "Debian XML/SGML Policy" "xml-sgml-policy">

<div class="centerblock">
<p>
  Linee guida per i pacchetti Debian che forniscono e/o fanno uso
  di risorse XML o SGML.
</p>
<doctable>
  <authors "Mark Johnson, Ardo van Rangelrooij, Adam Di Carlo">
  <status>
  È iniziata l'unione nelle attuali linee guida SGML di <tt>sgml-base-doc</tt>
  e di nuovi materiali per la gestione di documenti XML
  </status>
  <availability>
  <inddpsvn-xml-sgml-policy>
  </availability>
</doctable>
</div>
<hr>

<document "Debian Installer internals" "d-i-internals">

<div class="centerblock">
<p>
  Lo scopo di questo documento è di rendere più accessibile ai nuovi sviluppatori
  il Debian Installer e per raccogliere le informazioni tecniche ad esso relative.
</p>
<doctable>
  <authors "Frans Pop">
  <maintainer "Debian Installer team">
  <status>
  ready
  </status>
  <availability>
  <p><a href="https://d-i.debian.org/doc/internals/">HTML online</a></p>
  <p><a href="https://salsa.debian.org/installer-team/debian-installer/tree/master/doc/devel/internals">sorgenti DocBook XML online</a></p>
  </availability>
</doctable>
</div>

<hr>

<document "dbconfig-common documentation" "dbconfig-common">

<div class="centerblock">
<p>
  This document is intended for package maintainers who maintain packages that
  require a working database. Instead of implementing the required logic
  themselves they can rely on dbconfig-common to ask the right questions during
  install, upgrade, reconfigure and deinstall for them and create and fill the
  database.
</p>
<doctable>
  <authors "Sean Finney and Paul Gevers">
  <maintainer "Paul Gevers">
  <status>
  ready
  </status>
  <availability>
  <inpackage "dbconfig-common">
  <inddpsvn-dbconfig-common>
  Additional also the <a href="/doc/manuals/dbconfig-common/dbconfig-common-design.html">design document</a> is available.
  </availability>
</doctable>
</div>

<hr>

<document "dbapp-policy" "dbapp-policy">

<div class="centerblock">
<p>
  A proposed policy for packages that depend on a working database.
</p>
<doctable>
  <authors "Sean Finney">
  <maintainer "Paul Gevers">
  <status>
  draft
  </status>
  <availability>
  <inpackage "dbconfig-common">
  <inddpsvn-dbapp-policy>
  </availability>
</doctable>
</div>
