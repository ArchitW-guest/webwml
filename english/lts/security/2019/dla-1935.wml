<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Lilith of Cisco Talos discovered a buffer overflow flaw in the quota
code used by e2fsck from the ext2/ext3/ext4 file system utilities.
Running e2fsck on a malformed file system can result in the execution of
arbitrary code.</p>


<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1.42.12-2+deb8u1.</p>

<p>We recommend that you upgrade your e2fsprogs packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1935.data"
# $Id: $
