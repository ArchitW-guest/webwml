#use wml::debian::translation-check translation="8a5eb3970afae92528c267b43ff2cff70683b130" mindelta="1"
<define-tag description>säkerhetsuppdatering</define-tag>
<define-tag moreinfo>
<p>Flera sårbarheter har upptäckts i Linuxkärnan som kan leda
till utökning av privilegier, överbelastning eller informationsläckage.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3846">CVE-2019-3846</a>,
	<a href="https://security-tracker.debian.org/tracker/CVE-2019-10126">CVE-2019-10126</a>

	<p>huangwen rapporterade flera buffertspill i Marvell wifi-drivrutinen
	(mwifiex), vilka en lokal användare kunde använda för att orsaka
	överbelastning eller exekvering av godtycklig kod.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5489">CVE-2019-5489</a>

	<p>Daniel Gruss, Erik Kraft, Trishita Tiwari, Michael Schwarz, Ari
	Trachtenberg, Jason Hennessey, Alex Ionescu och Anders Fogh
	upptäckte att lokala användare kunde använda systemanropet mincore()
	för att få tag på känslig information från andra processer som har åtkomst
	till samma minnesmappade fil.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9500">CVE-2019-9500</a>,
	<a href="https://security-tracker.debian.org/tracker/CVE-2019-9503">CVE-2019-9503</a>

	<p>Hugues Anguelkov upptäckte ett buffertspill och saknad åtkomstvalidering
	i wifi-drivrutinen Broadcom FullMAC (brcmfmac), vilket en angripare på
	samma wifinätverk kunde använda för att orsaka överbelastning eller
	exekvering av godtycklig kod.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11477">CVE-2019-11477</a>

	<p>Jonathan Looney rapporterade att en speciellt skapad sekvens av
	TCP selective acknowledgements (SACKs) tillåter en från fjärran
	triggningsbar kärnpanik.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11478">CVE-2019-11478</a>

	<p>Jonathan Looney rapporterade att en speciellt skapad sekvens av
	TCP selective acknowledgements (SACKs) kommer att fragmentera
	TCP-återöverföringskön, vilket tillåter en angripare att orsaka
	överdriven resursanvändning.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11479">CVE-2019-11479</a>

	<p>Jonathan Looney rapporterade att en angripare kunde tvinga
	Linuxkärnan att segmentera sina svar till flera TCP-segment, av vilka
	varje innehåller endast 8 bytes data, vilket drastiskt ökar
	bandbreddskraven för att leverera samma mängd data.</p>
	
	<p>Denna uppdatering introducerar ett nytt sysctl-värde för att kontrollera
	den minimala MSS (net.ipv4.tcp_min_snd_mss), vilket som standard använder
	det tidigare hårdkodade värdet 48. Vi rekommenderar att öka detta till
	536 om du inte vet att ditt nätverk kräver ett lägre värde.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11486">CVE-2019-11486</a>

	<p>Jann Horn från Google rapporterade flera kapplöpningseffekter i
	Siemens R3964 linjedisciplinen. En lokal användare kunde använda dessa för
	att orsaka icke specificerad säkerhetspåverkan. Denna modul har därför
	inaktiverats.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11599">CVE-2019-11599</a>

	<p>Jann Horn från Google rapporterade en kapplöpningseffekter i
	implementationen av core dump vilket kunde leda till en användning efter
	frigörning. En lokal användare kunde använda detta för att läsa känslig
	information, för att orsaka en överbelastning (minneskonsumption), eller
	för rättighetseskalering.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11815">CVE-2019-11815</a>

	<p>Man har upptäckts att en användning efter frigörning i protocol
	Reliable Datagram Sockets kunde resultera i överbelastning och potentiellt
	utökning av privilegier. Denna protokollmodul (rds) laddas inte som standard
	på Debiansystem, så detta problem påverkar endast system där den uttryckligen
	laddas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11833">CVE-2019-11833</a>

	<p>Man har upptäckt att implementationen av ext4-filsystemet skriver
	icke initierad data från kärnminne till nya extent-block. En lokal
	användare som kan skriva till ett ext4-filsystem och sedan läsa
	filsystemsavbildning, exempelvis med hjälp av en portabel disk, kan
	ha möjlighet att använda detta för att få tag på känslig information.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11884">CVE-2019-11884</a>

	<p>Man har upptäckt att implementationen av Bluetooth HIDP inte säkerställer
	att nya anslutningsnamn var null-terminerade. En lokal användare med
	CAP_NET_ADMIN-möjligheten kan ha möjlighet att använda detta för att
	få tag på känslig information från kärnstacken.</p></li>

</ul>

<p>För den stabila utgåvan (Stretch) har dessa problem rättats i
version 4.9.168-1+deb9u3.</p>

<p>Vi rekommenderar att ni uppgraderar era linux-paket.</p>

<p>För detaljerad säkerhetsstatus om linux vänligen se
dess säkerhetsspårare på
<a href="https://security-tracker.debian.org/tracker/linux">https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4465.data"
