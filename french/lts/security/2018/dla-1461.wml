#use wml::debian::translation-check translation="ab3be4ee01879fd4484c795bbaa824377c218575" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>ClamAV, un utilitaire anti-virus pour Unix, a publié la version 0.100.1.
L’installation de cette nouvelle version est nécessaire pour utiliser toutes les
signatures de virus actuels et éviter les avertissements.</p>

<p>Cette version corrige aussi deux problèmes de sécurité découverts après la
version 0.100.0:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-0360">CVE-2018-0360</a>

<p>Dépassement d’entier avec une boucle infinie résultante à l'aide d'un
fichier contrefait du traitement de texte Hangul. Rapporté par Secunia Research
à Flexera.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-0361">CVE-2018-0361</a>

<p>Vérification de longueur d’objet PDF, durée excessive pour analyser un fichier
relativement petit. Rapporté par aCaB.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 0.100.1+dfsg-0+deb8u1.</p>
<p>Nous vous recommandons de mettre à jour vos paquets clamav.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify le following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1461.data"
# $Id: $
