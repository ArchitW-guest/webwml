<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>The UNIX pipe which sudo uses to contact SSSD and read the available sudo rules
from SSSD has too wide permissions, which means that anyone who can send a
message using the same raw protocol that sudo and SSSD use can read the sudo
rules available for any user.</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.11.7-3+deb8u1.</p>

<p>We recommend that you upgrade your sssd packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1429.data"
# $Id: $
