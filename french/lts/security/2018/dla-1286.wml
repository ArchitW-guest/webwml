#use wml::debian::translation-check translation="ec63203ae7c88b70ad002f6a6d89bd22297d4fb8" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités ont été découvertes dans Quagga, un démon de
routage. Le projet « Common Vulnerabilities and Exposures » (CVE) identifie
les problèmes suivants.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5378">CVE-2018-5378</a>

<p>Le démon BGP de Quagga, bgpd, ne vérifie pas correctement les limites
des données envoyées avec un « NOTIFY » à un pair, si une longueur
d'attribut n'est pas valable. Un pair BGP configuré peut tirer avantage de
ce bogue pour lire la mémoire du processus bgpd ou provoquer un déni de
service (plantage du démon).</p>

<p><url "https://www.quagga.net/security/Quagga-2018-0543.txt"></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5379">CVE-2018-5379</a>

<p>Le démon BGP de Quagga, bgpd, peut procéder à une double libération de
zone de mémoire lors du traitement de certaine formes de message
« UPDATE », contenant des attributs « cluster-list » ou inconnus, avec pour
conséquence, un déni de service (plantage du démon bgpd).</p>

 <p><url "https://www.quagga.net/security/Quagga-2018-1114.txt"></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5380">CVE-2018-5380</a>

<p>Le démon BGP de Quagga, bgpd, ne gère pas correctement les tables de
conversion, internes à BGP, de codes en chaînes de caractères.</p>

<p><url "https://www.quagga.net/security/Quagga-2018-1550.txt"></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5381">CVE-2018-5381</a>

<p>Le démon BGP de Quagga, bgpd, peut entrer dans une boucle infinie lors
de l'envoi d'un message OPEN non valable par un pair configuré. Un pair
configuré peut tirer avantage de ce défaut pour provoquer un déni de
service (le démon bgpd ne répond à aucun autre événement ; les sessions BGP
abandonnent et ne sont pas rétablies ; l'interface en ligne de commande ne
répond pas).</p>

 <p><url "https://www.quagga.net/security/Quagga-2018-1975.txt"></p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 0.99.22.4-1+wheezy3+deb7u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets quagga.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1286.data"
# $Id: $
