# German translation of the Debian webwml modules
# Copyright (c) 2004 Software in the Public Interest, Inc.
# Jens Seidel <tux-master@web.de>, 2004.
# Dr. Tobias Quathamer <toddy@debian.org>, 2005.
# Holger Wansing <linux@wansing-online.de>, 2011.
# Holger Wansing <hwansing@mailbox.org>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml langs\n"
"PO-Revision-Date: 2019-02-21 20:27+0100\n"
"Last-Translator: Holger Wansing <hwansing@mailbox.org>\n"
"Language-Team: Debian l10n German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/template/debian/language_names.wml:8
msgid "Arabic"
msgstr "Arabisch"

#: ../../english/template/debian/language_names.wml:9
msgid "Armenian"
msgstr "Armenisch"

#: ../../english/template/debian/language_names.wml:10
msgid "Finnish"
msgstr "Finnisch"

#: ../../english/template/debian/language_names.wml:11
msgid "Croatian"
msgstr "Kroatisch"

#: ../../english/template/debian/language_names.wml:12
msgid "Danish"
msgstr "Dänisch"

#: ../../english/template/debian/language_names.wml:13
msgid "Dutch"
msgstr "Niederländisch"

#: ../../english/template/debian/language_names.wml:14
msgid "English"
msgstr "Englisch"

#: ../../english/template/debian/language_names.wml:15
msgid "French"
msgstr "Französisch"

#: ../../english/template/debian/language_names.wml:16
msgid "Galician"
msgstr "Galizisch"

#: ../../english/template/debian/language_names.wml:17
msgid "German"
msgstr "Deutsch"

#: ../../english/template/debian/language_names.wml:18
msgid "Italian"
msgstr "Italienisch"

#: ../../english/template/debian/language_names.wml:19
msgid "Japanese"
msgstr "Japanisch"

#: ../../english/template/debian/language_names.wml:20
msgid "Korean"
msgstr "Koreanisch"

#: ../../english/template/debian/language_names.wml:21
msgid "Spanish"
msgstr "Spanisch"

#: ../../english/template/debian/language_names.wml:22
msgid "Portuguese"
msgstr "Portugiesisch"

#: ../../english/template/debian/language_names.wml:23
msgid "Portuguese (Brazilian)"
msgstr "Portugiesisch (Brasilianisch)"

#: ../../english/template/debian/language_names.wml:24
msgid "Chinese"
msgstr "Chinesisch"

#: ../../english/template/debian/language_names.wml:25
msgid "Chinese (China)"
msgstr "Chinesisch (China)"

#: ../../english/template/debian/language_names.wml:26
msgid "Chinese (Hong Kong)"
msgstr "Chinesisch (Hong Kong)"

#: ../../english/template/debian/language_names.wml:27
msgid "Chinese (Taiwan)"
msgstr "Chinesisch (Taiwan)"

#: ../../english/template/debian/language_names.wml:28
msgid "Chinese (Traditional)"
msgstr "Chinesisch (Traditionell)"

#: ../../english/template/debian/language_names.wml:29
msgid "Chinese (Simplified)"
msgstr "Chinesisch (Vereinfacht)"

#: ../../english/template/debian/language_names.wml:30
msgid "Swedish"
msgstr "Schwedisch"

#: ../../english/template/debian/language_names.wml:31
msgid "Polish"
msgstr "Polnisch"

#: ../../english/template/debian/language_names.wml:32
msgid "Norwegian"
msgstr "Norwegisch"

#: ../../english/template/debian/language_names.wml:33
msgid "Turkish"
msgstr "Türkisch"

#: ../../english/template/debian/language_names.wml:34
msgid "Russian"
msgstr "Russisch"

#: ../../english/template/debian/language_names.wml:35
msgid "Czech"
msgstr "Tschechisch"

#: ../../english/template/debian/language_names.wml:36
msgid "Esperanto"
msgstr "Esperanto"

#: ../../english/template/debian/language_names.wml:37
msgid "Hungarian"
msgstr "Ungarisch"

#: ../../english/template/debian/language_names.wml:38
msgid "Romanian"
msgstr "Rumänisch"

#: ../../english/template/debian/language_names.wml:39
msgid "Slovak"
msgstr "Slowakisch"

#: ../../english/template/debian/language_names.wml:40
msgid "Greek"
msgstr "Griechisch"

#: ../../english/template/debian/language_names.wml:41
msgid "Catalan"
msgstr "Katalanisch"

#: ../../english/template/debian/language_names.wml:42
msgid "Indonesian"
msgstr "Indonesisch"

#: ../../english/template/debian/language_names.wml:43
msgid "Lithuanian"
msgstr "Litauisch"

#: ../../english/template/debian/language_names.wml:44
msgid "Slovene"
msgstr "Slowenisch"

#: ../../english/template/debian/language_names.wml:45
msgid "Bulgarian"
msgstr "Bulgarisch"

#: ../../english/template/debian/language_names.wml:46
msgid "Tamil"
msgstr "Tamil"

#. for now, the following are only needed if you intend to translate intl/l10n
#: ../../english/template/debian/language_names.wml:48
msgid "Afrikaans"
msgstr "Afrikaans"

#: ../../english/template/debian/language_names.wml:49
msgid "Albanian"
msgstr "Albanisch"

#: ../../english/template/debian/language_names.wml:50
msgid "Asturian"
msgstr "Asturisch"

#: ../../english/template/debian/language_names.wml:51
msgid "Amharic"
msgstr "Amharisch"

#: ../../english/template/debian/language_names.wml:52
msgid "Azerbaijani"
msgstr "Aserbaidschanisch"

#: ../../english/template/debian/language_names.wml:53
msgid "Basque"
msgstr "Baskisch"

#: ../../english/template/debian/language_names.wml:54
msgid "Belarusian"
msgstr "Weißrussisch"

#: ../../english/template/debian/language_names.wml:55
msgid "Bengali"
msgstr "Bengali"

#: ../../english/template/debian/language_names.wml:56
msgid "Bosnian"
msgstr "Bosnisch"

#: ../../english/template/debian/language_names.wml:57
msgid "Breton"
msgstr "Bretonisch"

#: ../../english/template/debian/language_names.wml:58
msgid "Cornish"
msgstr "Kornisch"

#: ../../english/template/debian/language_names.wml:59
msgid "Estonian"
msgstr "Estonisch"

#: ../../english/template/debian/language_names.wml:60
msgid "Faeroese"
msgstr "Färöisch"

#: ../../english/template/debian/language_names.wml:61
msgid "Gaelic (Scots)"
msgstr "Gälisch"

#: ../../english/template/debian/language_names.wml:62
msgid "Georgian"
msgstr "Georgisch"

#: ../../english/template/debian/language_names.wml:63
msgid "Hebrew"
msgstr "Hebräisch"

#: ../../english/template/debian/language_names.wml:64
msgid "Hindi"
msgstr "Hindi"

#: ../../english/template/debian/language_names.wml:65
msgid "Icelandic"
msgstr "Isländisch"

#: ../../english/template/debian/language_names.wml:66
msgid "Interlingua"
msgstr "Interlingua"

#: ../../english/template/debian/language_names.wml:67
msgid "Irish"
msgstr "Irisch"

#: ../../english/template/debian/language_names.wml:68
msgid "Kalaallisut"
msgstr "Kalaallisut"

#: ../../english/template/debian/language_names.wml:69
msgid "Kannada"
msgstr "Kanaresisch"

#: ../../english/template/debian/language_names.wml:70
msgid "Kurdish"
msgstr "Kurdisch"

#: ../../english/template/debian/language_names.wml:71
msgid "Latvian"
msgstr "Lettisch"

#: ../../english/template/debian/language_names.wml:72
msgid "Macedonian"
msgstr "Mazedonisch"

#: ../../english/template/debian/language_names.wml:73
msgid "Malay"
msgstr "Malaysisch"

#: ../../english/template/debian/language_names.wml:74
msgid "Malayalam"
msgstr "Malayalam"

#: ../../english/template/debian/language_names.wml:75
msgid "Maltese"
msgstr "Maltesisch"

#: ../../english/template/debian/language_names.wml:76
msgid "Manx"
msgstr "Manx"

#: ../../english/template/debian/language_names.wml:77
msgid "Maori"
msgstr "Maori"

#: ../../english/template/debian/language_names.wml:78
msgid "Mongolian"
msgstr "Mongolisch"

#: ../../english/template/debian/language_names.wml:80
msgid "Norwegian Bokm&aring;l"
msgstr "Bokmål-Norwegisch"

#: ../../english/template/debian/language_names.wml:82
msgid "Norwegian Nynorsk"
msgstr "Nynorsk-Norwegisch"

#: ../../english/template/debian/language_names.wml:83
msgid "Occitan (post 1500)"
msgstr "Okzitanisch"

#: ../../english/template/debian/language_names.wml:84
msgid "Persian"
msgstr "Persisch"

#: ../../english/template/debian/language_names.wml:85
msgid "Serbian"
msgstr "Serbisch"

#: ../../english/template/debian/language_names.wml:86
msgid "Slovenian"
msgstr "Slowenisch"

#: ../../english/template/debian/language_names.wml:87
msgid "Tajik"
msgstr "Tadschikisch"

#: ../../english/template/debian/language_names.wml:88
msgid "Thai"
msgstr "Thailändisch"

#: ../../english/template/debian/language_names.wml:89
msgid "Tonga"
msgstr "Tongaisch"

#: ../../english/template/debian/language_names.wml:90
msgid "Twi"
msgstr "Twi"

#: ../../english/template/debian/language_names.wml:91
msgid "Ukrainian"
msgstr "Ukrainisch"

#: ../../english/template/debian/language_names.wml:92
msgid "Vietnamese"
msgstr "Vietnamesisch"

#: ../../english/template/debian/language_names.wml:93
msgid "Welsh"
msgstr "Walisisch"

#: ../../english/template/debian/language_names.wml:94
msgid "Xhosa"
msgstr "Xhosa"

#: ../../english/template/debian/language_names.wml:95
msgid "Yiddish"
msgstr "Yiddisch"

#: ../../english/template/debian/language_names.wml:96
msgid "Zulu"
msgstr "Zulu"
