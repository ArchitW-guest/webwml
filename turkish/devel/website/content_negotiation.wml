#use wml::debian::template title="Content Negotiation"
#Translator: Murat Demirten <murat@debian.org>
#use wml::debian::translation-check translation="c25132d79dab5dda5298236044fc2bd05968ef47"

<H3>Web sunucu hangi dosyanın verileceğine nasıl karar verir</H3>
<P>Web sayfamızdaki çoğu bağlantının .html ile bitmediğini
farketmiş olmalısınız. Bu durum sayfaların hangi dildeki
versiyonlarının sunulacağına karar verilebilmesini sağlayan content
negotiation tekniğini kullanmamızdan kaynaklanmaktadır. Sayfanın
birden fazla versiyonu olduğu zaman sunucu tüm olası seçimleri
listeler. Sözgelimi eğer about sayfasına bir istek gelirse sunucu
about.en.html ve about.tr.html şeklinde listeleme yapabilir. Öntanımlı
olarak sayfalar İngilizce olarak sunulmakla birlikte istendiği takdirde
değiştirilebilir.

<P>Eğer web tarayıcınız üzerindeki dil değişkenleri düzgün bir
şekilde ayarlanmışsa, örneğin Türkçe için ayarlıysa, yukarıdaki
örnekte size about.tr.html sayfası sunulmaktadır. Bu yaklaşımın
bir diğer avantajı, tercih edilen dilde uygun bir sayfa yoksa
sayfanın öntanımlı dil versiyonunun ekrana getirilmesidir.
Sunucunun bu kararları verirken nasıl davrandığına ilişkin daha
fazla teknik bilgilere
<a href="https://httpd.apache.org/docs/current/content-negotiation.html">https://httpd.apache.org/docs/current/content-negotiation.html</a>
adresinden erişebilirsiniz.

<P>Her kullanıcının content-negotiation tekniğinden haberdar
olamayacağını düşünerek, tüm sayfaların altına, o sayfanın mevcut
olduğu diğer dillere ait bağlantıları da gösterdik. Sayfaların bu
kısımları wml tarafından çağrılan bir perl programı ile otomatik
olarak üretilmektedir.
